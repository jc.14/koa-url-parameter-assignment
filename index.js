const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const fs = require('fs')

const app = new Koa()
const router = new Router()

// Read file
function readExternalFile(FileName) {
    return new Promise(function(resolve, reject) {
      fs.readFile(FileName, 'utf8', function(err, newsData) {
        if (err)
          reject(err)
        else
          resolve(newsData)
      })
    })
}

// Read Individual News Json
async function readFile(id) {
    try {
        let datacontent = await readExternalFile('public/data/news-' + id +'.json');
        return datacontent
    } catch (error) {
        console.error(error)
    }
}

// Get all news' file name from directory
function readDir(dirname) {
    return new Promise(function(resolve,reject) {
        fs.readdir(dirname, function(err, filenames) {
            if(err) {
                reject(err)
            } else {
                resolve(filenames)
            }
        })
    })
}

// Read all News' Json file + Parsing at here
async function getAllNews(dirname) {
    let allNews = []
    try {
        let filenames = await readDir(dirname)
        for(const filename of filenames) {
            let tempNews = await readExternalFile(dirname+filename)
            allNews.push(JSON.parse(tempNews))
        }
        return allNews
    } catch (error) {
        console.log(error)
    }
}

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/', async ctx => {
    let allNews = await getAllNews('public/data/')
    const data = {
        title: '{JCEE} Shots! | You know nothing Mr.Snow!',
        news: allNews
    } 
    await ctx.render('landing', data)
})

router.get('/news/:id', async (ctx, next) => {
    let getNewsDetail = await readFile(ctx.params.id)
    let imgCol = await readDir('./public/images/news_image/' + ctx.params.id) // Get news' image file name
    let newsDetail = JSON.parse(getNewsDetail);
    newsDetail.newsImg = imgCol // Adding img file name array to newsDetail
    console.log(newsDetail)
    await ctx.render('news', newsDetail)
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)