const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

router.get('/', ctx => {
    ctx.body = 'This is index page.'
})

router.get('/about', ctx => {
    ctx.body = 'Hi! I am Jia.'
})

router.get('/contact', ctx => {
    ctx.body = 'Tell me now.'
})

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/landing', async ctx => {
    const data = {
        title: 'Landing page',
        users: [
          { name: 'John', age: 29 },
          { name: 'Thor', age: 1500 }
        ],
        animals: ['cat', 'dog']
      }     
    await ctx.render('landing', data)
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)